<?php
    $navigation_items = template_get_navigation_items();
?>
<ul class="navbar-nav mr-auto">
    <?php
        foreach ($navigation_items as $item) {
            ?>
            <li class="nav-item active">
                <a class="nav-link" href="termekek.php?katid=<?php echo $item['id']?>">
                    <?php echo $item['katnev']; ?>
                </a>
            </li>
            <?php
        }
    ?>
</ul>
