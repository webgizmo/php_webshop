<header>
    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <a class="navbar-brand" href="/">Gzu</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="termekek.php">
                        Termekek
                    </a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="kosar.php">
                        Kosaram
                    </a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="tudnivalok.php">
                        Tudnivalok
                    </a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="kapcsolat.php">
                        Kapcsolat
                    </a>
                </li>

            </ul>
            <form class="form-inline mt-2 mt-md-0" action="kereses.php" method="get">
                <input class="form-control mr-sm-2" type="text" name="kereses" value="<?=$_GET['kereses']?>" placeholder="Search" aria-label="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
            </form>
        </div>
    </nav>
</header>