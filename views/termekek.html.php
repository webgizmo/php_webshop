<?php
    foreach($termekek as $termek):
?>
<div class="termekdoboz">

    <div class="termekkep">
        <a href="termek.php?termekid=<?=$termek['id']?>">
            <img src="./assets/<?=$termek['kep']?>" alt="" title="" /> </a>
    </div>

    <div class="termeknev"><?=$termek['nev']?></div>
    <div class="termekar"> (<?php echo number_format($termek['ar']);?>) Ft </div>

    <div class="termekkosar">
        <a href="kosarmuvelet.php?id=<?=$termek['id']?>"&action=add">Kosárba</a>
    </div>

</div>
<?php endforeach; ?>