<?php

class Megjelenito
{
    public $termekek = [];

    function setTermekek($termekek) {
        $this->termekek = $termekek;
    }

    function renderTermekek() {
        foreach($this->termekek as $termek) {
            echo "
                <div class='termekdoboz' style='padding: 20px; border: solid 1px red;'>
                    
                    <div class='termekkep'>
                        <a href='termek.php?termekid=".$termek['id']."'>".$termek['nev']." </a>
                    </div>
    
                    <div class='termeknev'>".$termek['nev']."</div>
                </div>
            ";
        }
    }
}
