<?php


class Adatbazis{
    private $host;
    private $user;
    private $password;
    private $database;

    private $connection;

    function connect($host, $user, $password, $database) {
        $this->connection = mysqli_connect($host ,$user, $password, $database);
        mysqli_query($this->connection, "SET NAMES utf8");

        $this->host = $host;
        $this->user = $user;
        $this->password = $password;
        $this->database = $database;
    }

    function runSql($query){
        $query_results = mysqli_query($this->connection, $query);

        $result = [];

        while($row = mysqli_fetch_array($query_results)) {
            $result[] = [
                'id'=> $row['id'],
                'nev' => $row['nev']
            ];
        }
        return $result;
    }
}