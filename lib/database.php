<?php

if (!defined('APP_RUNNING')) {
    exit('You have no power here!');
}

/*
 * Egyszeru fuggvany ami kapcsolodik az adatbazishoz es
 * visszater az adatbazis kapcsolati objektummal
 * */
function get_db()
{
    $con = mysqli_connect(DB_HOST ,DB_USER, DB_PASSWORD, DB_DATABASE);
    mysqli_query($con, "SET NAMES utf8");

    return $con;
}

/*
 * Adott sql query-t az adott adatbazis kapcsolaton lefuttat
 * ha nem adunk adatbazis kapcsolatot akkor lekerdezi maganak
 * pl. $eredmeny = run_sql('select * from termekek', $db); -> igy mi adjuk a kapcsolatot
 * $eredmeny = run_sql('select * from termekek'); -> igy a run_sql maganak keri le a kapcsolatot
 * */
function run_sql($sql, $db = null)
{
    if (is_null($db)) {
        $db = get_db();
    }
    $results = [];

    /*
    * Itt kuldjuk el az SQL-t a szervernek
    * */
    $query_results = mysqli_query($db, $sql);

    /*
    * Ez itt egy kis trukk, a field_infoban benne lesz hogy milyen
    * mezok vannak az sql-ben
    * alapbol a mysqli eleg csunyan kuldi vissza az adatokat
    * a tombben benne vannak indexelve meg szoveges indexxel is...
     * nem tul hasznalhato
    * */
    $field_info = mysqli_fetch_fields($query_results);

    // vedd le innen a kommentet es hasonlitsd ossze a query_results
    // es a feldolgozott $results kozotti kulonbsegeket
    //dd($query_results);

    while($row = mysqli_fetch_array($query_results)) {
        $result_item = [];
        foreach ($field_info as $info) {
            $result_item[$info->name] = $row[$info->name];
        }
        $results[] = $result_item ;
    }

    //dd($results);

    return $results;
}