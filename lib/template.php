<?php

if (!defined('APP_RUNNING')) {
    exit('You have no power here!');
}

function template_get_navigation_items()
{
    $db = get_db();

    $nav_items = run_sql('select * from kategoriak order by katsorrend DESC',$db);

    return $nav_items;
}

function is_get_request()
{
    if (
        (empty($_GET) && empty($_POST))
        ||
        !empty($_GET)
    ) {
        return true;
    } else {
        return false;
    }
}

function is_post_request()
{
    if (!empty($_POST)) {
        return true;
    } else {
        return false;
    }
}



function html($file, $variables = array(), $layout = '')
{
    if (!is_file($file)) {
        die('Missing view file');
    }

    ob_start();
    extract($variables);
    include $file;
    $content = ob_get_clean();


    if (empty($layout)) return $content;

    if (!is_file($layout)) {
        die('Missing layout file');
    }
    return html($layout, array('content' => $content));
}


/**
 * An alias of {@link htmlspecialchars()}.
 * If no $charset is provided, uses option('encoding') value
 *
 * @param string $str
 * @param string $quote_style
 * @param string $charset
 * @return void
 */
function h($str, $quote_style = ENT_NOQUOTES, $charset = null)
{
    if (is_null($charset)) $charset = 'UTF-8';
    return htmlspecialchars($str, $quote_style, $charset);
}


