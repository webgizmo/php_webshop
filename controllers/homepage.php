<?php

function render_homepage() {
    echo html(
        'views/homepage.html.php',
        array(),
        'views/layouts/no_sidebar.html.php');
}

function process_homepage() {

}

function process_kereses()
{
    $kulcsszo = $_GET['kereses'];

    $termekek = run_sql("select * from termekek where nev LIKE '%".$kulcsszo."%'");

    echo html(
        'views/termekek.html.php',
        array('termekek'=>$termekek),
        'views/layouts/default.html.php');
}
