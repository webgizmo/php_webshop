<?php
/*
 * egy file mind felett, eleg a boot.php-t behivni es az minden
 * szukseges kiegeszito dolgot elintez
 *
 * elinditja a session-t
 * */
session_start();
define('APP_RUNNING',true);

/*
 * Behivja a config kostansokat
 * */
include_once 'core/_config.php';

/*
 * Az autoloader betolti a contollers es lib mappakban levo
 * php filekoat, igy a bennuk levo fuggvenyek elerhetoek lesznek
 * */
include_once 'core/_autoloader.php';

