<?php

if (!defined('APP_RUNNING')) {
    exit('You have no power here!');
}

$folders = array('lib','controllers');

/*
 * Vegigmegyunk a $folders tombben levo elemeken, az aktualis ciuklusban
 * a folder valtozoban lesz a konyvtar: lib vagy controllers
 * */
foreach ($folders as $folder) {

    /*
     * scandir kilistazza az adott konyvtarban levo dolgokat
     * */
    $files = scandir($folder);

    /*
     * vegigmegyunk a fileokon, az adott file a file nevu valtozoba kerul
     * */
    foreach ($files as $file) {

        /*
         * Linux hulyeseg, a . es .. megjelennek a fajlok kozott
         * de ok nem valodi fajlok, igy ignoraljuk oket
         * */
        if ($file != '.' && $file != '..') {

            /*
             * ha valos fajl, akkor include_once-al behivjuk
             * */
            include_once $folder.'/'.$file;
        }
    }
}


